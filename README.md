# FileBrowser

WPF user control for file selection.

Its main aim is to provide a way from opening and/or creating a file with the same component, going around OpenFileDialog which doesn't want a new file and SaveFileDialog which will ask to overwrite.

## References

- [ReactiveUI](https://github.com/reactiveui/ReactiveUI): composable, cross-platform model-view-viewmodel framework for all .NET platforms that is inspired by functional reactive programming.
- [MaterialDesignInXAML](https://github.com/MaterialDesignInXAML/MaterialDesignInXamlToolkit): Comprehensive and easy to use Material Design theme and control library for the Windows desktop.
