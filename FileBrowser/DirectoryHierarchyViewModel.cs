﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace FileBrowser
{
    internal enum Category
    {
        Unknown,
        ClosedDirectory,
        OpenedDirectory,
        Drive,
        Network,
        Desktop,
        Documents,
        Music,
        Pictures,
        Videos
    }

    public class DirectoryHierarchyViewModel : ReactiveObject, IEquatable<DirectoryHierarchyViewModel>, IActivatableViewModel
    {
        [Reactive] public string Name { get; set; }
        [Reactive] public bool IsDrive { get; set; }
        [Reactive] public bool IsSpecialFolder { get; set; }
        [Reactive] internal Category SpecialCategory { get; set; }
        [Reactive] public bool Expanded { get; set; }
        [Reactive] public bool IsSelected { get; set; }

        [Reactive] private DirectoryHierarchyViewModel Parent { get; set; }
        public DirectoryHierarchyViewModel[] Children { [ObservableAsProperty] get; }
        public string DisplayName { [ObservableAsProperty] get; }
        internal Category Category { [ObservableAsProperty] get; }

        private bool _hasProcessed;

        private ReactiveCommand<Unit , DirectoryHierarchyViewModel[]> FindChildrenCommand { get; }

        public DirectoryHierarchyViewModel()
        {
            Activator = new ViewModelActivator();

            FindChildrenCommand = ReactiveCommand.CreateFromObservable( () =>
                Observable.Start( () => Directory.EnumerateDirectories( Name )
                    .Where( d => !new DirectoryInfo( d ).Attributes.HasFlag( FileAttributes.Hidden ) )
                    .Select( d => new DirectoryHierarchyViewModel( this ) { Name = d } )
                    .ToArray() ) );
            FindChildrenCommand.ThrownExceptions.Subscribe( exc =>
                this.Log().Error( "Couldn't list children: {0}" , exc ) );

            this.WhenAnyValue( x => x.IsDrive )
                .CombineLatest( this.WhenAnyValue( x => x.IsSpecialFolder ) )
                .Where( x => x.First || x.Second )
                .Throttle( TimeSpan.FromMilliseconds( 100 ) )
                .Select( _ => Unit.Default )
                .InvokeCommand( FindChildrenCommand );

            FindChildrenCommand
                .Do( _ => _hasProcessed = true )
                .ToPropertyEx( this , x => x.Children , scheduler: RxApp.MainThreadScheduler );

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.Name ).Where( x => !string.IsNullOrWhiteSpace( x ) )
                                 .Throttle( TimeSpan.FromMilliseconds( 10 ) )
                                 .Select( name => new DirectoryInfo( name ).Name )
                                 .ToPropertyEx( this , x => x.DisplayName , scheduler: RxApp.MainThreadScheduler )
                                 .DisposeWith( disposables );

                Observable.CombineLatest(
                    this.WhenAnyValue( x => x.Name ) ,
                    this.WhenAnyValue( x => x.Parent ) ,
                    this.WhenAnyValue( x => x.IsDrive ) ,
                    this.WhenAnyValue( x => x.IsSpecialFolder ) ,
                    this.WhenAnyValue( x => x.Expanded ) ,
                    ( name , parent , isDrive , isSpecial , isExpanded ) => (name, parent, isDrive, isSpecial, isExpanded) )
                    .Throttle( TimeSpan.FromMilliseconds( 20 ) )
                    .Select( t =>
                    {
                        var (name, parent, isDrive, isSpecial, isExpanded) = t;

                        if ( name.StartsWith( @"\\" ) && parent == null )
                            return Category.Network;

                        if ( isSpecial )
                            return SpecialCategory;

                        if ( isDrive )
                            return Category.Drive;

                        return isExpanded ? Category.OpenedDirectory : Category.ClosedDirectory;
                    } )
                    .ToPropertyEx( this , x => x.Category , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );
            } );
        }

        public DirectoryHierarchyViewModel( DirectoryHierarchyViewModel parent ) : this()
        {
            Parent = parent;

            parent.WhenAnyValue( x => x.Expanded )
                .Where( x => x && !_hasProcessed )
                .Throttle( TimeSpan.FromMilliseconds( 100 ) )
                .Select( _ => Unit.Default )
                .InvokeCommand( FindChildrenCommand );
        }

        public DirectoryHierarchyViewModel[] Paths =>
            Parent != null ? Parent.Paths.Append( this ).ToArray() : new[] { this };

        public IEnumerable<DirectoryHierarchyViewModel> GetHierarchy()
        {
            yield return this;
            if ( Children != null )
                foreach ( var child in Children )
                    foreach ( var cc in child.GetHierarchy() )
                        yield return cc;
        }

        public ViewModelActivator Activator { get; }

        public override string ToString()
            => $"{Name}";

        public bool Equals( DirectoryHierarchyViewModel other )
        {
            if ( ReferenceEquals( null , other ) ) return false;
            if ( ReferenceEquals( this , other ) ) return true;
            return Name == other.Name && IsDrive == other.IsDrive && Equals( Parent , other.Parent );
        }

        public override bool Equals( object obj )
        {
            if ( ReferenceEquals( null , obj ) ) return false;
            if ( ReferenceEquals( this , obj ) ) return true;
            if ( obj.GetType() != this.GetType() ) return false;
            return Equals( (DirectoryHierarchyViewModel) obj );
        }

        public override int GetHashCode()
        {
            return HashCode.Combine( Name , IsDrive , Parent );
        }
    }

    internal static class DirectoryHierarchyViewExtensions
    {
        internal static IEnumerable<DirectoryHierarchyViewModel> ListAll( this IEnumerable<DirectoryHierarchyViewModel> directories )
        {
            foreach ( var directory in directories )
                foreach ( var element in directory.GetHierarchy() )
                    yield return element;
        }
    }
}