﻿using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace FileBrowser
{
    public partial class PathSelectorView
    {
        public PathSelectorView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( o => PopulateFromViewModel( this , o , disposables ) )
                    .Subscribe()
                    .DisposeWith( disposables )

            );
        }

        private static void PopulateFromViewModel( PathSelectorView view , PathSelectorViewModel viewModel ,
            CompositeDisposable disposables )
        {
            view.IsHitTestVisible = viewModel.IsHitTestVisible;

            view.OneWayBind( viewModel ,
                    vm => vm.DirectoryHierarchyViewModel.DisplayName ,
                    v => v.Chip.Content )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.SelectDirectoryCommand ,
                    v => v.Chip )
                .DisposeWith( disposables );
        }
    }
}