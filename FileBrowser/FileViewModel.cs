﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.IO;
using System.Reactive.Linq;

namespace FileBrowser
{
    public class FileViewModel : ReactiveObject, IActivatableViewModel
    {
        [Reactive] public string Name { get; set; }
        [Reactive] public bool IsSelected { get; set; }

        [Reactive] public string DisplayName { get; set; }
        [Reactive] public string Extension { get; set; }
        [Reactive] public DateTime LastTime { get; set; }
        [Reactive] public long Size { get; set; }

        public ViewModelActivator Activator { get; }

        public FileViewModel()
        {
            Activator = new ViewModelActivator();

            this.WhenAnyValue( x => x.Name ).WhereNotNull()
                .Throttle( TimeSpan.FromMilliseconds( 10 ) )
                .Subscribe( name =>
                {
                    var fi = new FileInfo( name );
                    DisplayName = fi.Name;
                    Extension = fi.Extension;
                    LastTime = fi.LastAccessTime;
                    Size = fi.Length;
                } );

            //this.WhenActivated( disposables =>
            //{
            //        .DisposeWith( disposables );
            //} );
        }
    }
}