using ReactiveUI;
using Splat;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FileBrowser
{
    public partial class FileBrowserPane
    {
        static FileBrowserPane()
        {
            Locator.CurrentMutable.Register( () => new DirectoryHierarchyView() , typeof( IViewFor<DirectoryHierarchyViewModel> ) );
        }

        public FileBrowserPane()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( o => PopulateFromViewModel( this , o , disposables ) )
                    .Subscribe()
                    .DisposeWith( disposables )
                );

            ViewModel = new FileBrowserViewModel();
        }

        private static void PopulateFromViewModel( FileBrowserPane view , FileBrowserViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                    vm => vm.Directories ,
                    v => v.DirectoriesViewer.ItemsSource )
                .DisposeWith( disposables );

            view.DirectoriesViewer.Events()
                .SelectedItemChanged
                .Subscribe( e =>
                {
                    viewModel.SelectedDirectory = e.NewValue as DirectoryHierarchyViewModel;
                } )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                    vm => vm.SelectedFile ,
                    v => v.FilesViewer.SelectedItem )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.Files ,
                    v => v.FilesViewer.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.Paths ,
                    v => v.PathsControl.ItemsSource )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                    vm => vm.LookupPath ,
                    v => v.TextBoxLookupPath.Text )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.FindPathCommand ,
                    v => v.ButtonLookup ,
                    viewModel.WhenAnyValue( x => x.LookupPath ) )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.SelectedDirectory ,
                    v => v.TextBlockPath.Text ,
                    sd => sd?.Name ?? string.Empty )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                    vm => vm.FileInputName ,
                    v => v.TextBoxFile.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.Mode ,
                    v => v.TextBoxFile.IsReadOnly ,
                    m => m == BrowserMode.ReadOnly )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.Mode ,
                    v => v.TextBoxFile.IsHitTestVisible ,
                    m => m == BrowserMode.Create )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                    vm => vm.LookupFile ,
                    v => v.TextBoxLookupFile.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.HasNoFile ,
                    v => v.BorderNoFile.Visibility ,
                    b => b ? Visibility.Visible : Visibility.Collapsed )
                .DisposeWith( disposables );
        }
    }
}