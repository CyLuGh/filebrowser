﻿using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace FileBrowser
{
    public partial class DirectoryHierarchyView
    {
        public DirectoryHierarchyView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .Subscribe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( DirectoryHierarchyView view , DirectoryHierarchyViewModel viewModel ,
            CompositeDisposable disposable )
        {
            view.OneWayBind( viewModel ,
                    vm => vm.DisplayName ,
                    v => v.TextBlockDisplayName.Text )
                .DisposeWith( disposable );

            view.OneWayBind( viewModel ,
                    vm => vm.Category ,
                    v => v.Icon.Kind ,
                    cat => cat switch
                    {
                        Category.Network => PackIconKind.FolderNetworkOutline,
                        Category.Drive => PackIconKind.Harddisk,
                        Category.ClosedDirectory => PackIconKind.FolderOutline,
                        Category.OpenedDirectory => PackIconKind.FolderOpenOutline,
                        Category.Desktop => PackIconKind.DesktopMac,
                        Category.Documents => PackIconKind.FileDocumentMultipleOutline,
                        Category.Pictures => PackIconKind.ImageMultipleOutline,
                        Category.Videos => PackIconKind.VideoOutline,
                        Category.Music => PackIconKind.MusicBoxMultipleOutline,
                        _ => PackIconKind.ProgressQuestion
                    } )
                .DisposeWith( disposable );
        }
    }
}