﻿using ReactiveUI;
using System.Reactive;

namespace FileBrowser
{
    public class PathSelectorViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly FileBrowserViewModel _fileBrowserViewModel;
        public DirectoryHierarchyViewModel DirectoryHierarchyViewModel { get; }
        public ViewModelActivator Activator { get; }

        public ReactiveCommand<Unit , Unit> SelectDirectoryCommand { get; set; }
        public bool IsHitTestVisible { get; set; }

        public PathSelectorViewModel( FileBrowserViewModel fileBrowserViewModel , DirectoryHierarchyViewModel directoryHierarchyViewModel )
        {
            _fileBrowserViewModel = fileBrowserViewModel;
            DirectoryHierarchyViewModel = directoryHierarchyViewModel;
            Activator = new ViewModelActivator();

            IsHitTestVisible = !directoryHierarchyViewModel.IsSelected;

            InitializeCommands( this );
        }

        private static void InitializeCommands( PathSelectorViewModel @this )
        {
            @this.SelectDirectoryCommand = ReactiveCommand.Create( () =>
            {
                @this._fileBrowserViewModel.SelectedDirectory = @this.DirectoryHierarchyViewModel;
            } );
        }
    }
}