﻿using System;
using System.IO;

namespace FileBrowser
{
    public static class StringHelper
    {
        public static int CommonStartingCharacters( this string first , string second )
        {
            int res = 0;

            for ( int pos = 0 ; pos < first.Length ; pos++ )
            {
                if ( pos >= second.Length )
                    break;

                if ( string.Compare( first.Substring( 0 , pos + 1 ) , second.Substring( 0 , pos + 1 ) ,
                    StringComparison.CurrentCultureIgnoreCase ) == 0 )
                    res++;
                else break;
            }

            return res;
        }

        public static string NormalizePath( this string path )
        {
            return Path.GetFullPath( new Uri( path ).LocalPath )
                .TrimEnd( Path.DirectorySeparatorChar , Path.AltDirectorySeparatorChar )
                .ToUpperInvariant();
        }
    }
}