﻿using System.Text.RegularExpressions;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Splat;

namespace FileBrowser
{
    public enum BrowserMode { ReadOnly, Create }

    public class FileBrowserViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        private SourceCache<DirectoryHierarchyViewModel , string> DirectoriesCache { get; }
            = new SourceCache<DirectoryHierarchyViewModel , string>( o => o.Name );

        private ReadOnlyObservableCollection<DirectoryHierarchyViewModel> _directories;
        internal ReadOnlyObservableCollection<DirectoryHierarchyViewModel> Directories => _directories;

        private SourceCache<FileViewModel , string> FileCache { get; }
            = new SourceCache<FileViewModel , string>( o => o.Name );

        private ReadOnlyObservableCollection<FileViewModel> _files;
        internal ReadOnlyObservableCollection<FileViewModel> Files => _files;

        [Reactive] public string FilterRegex { get; set; }
        [Reactive] public BrowserMode Mode { get; set; }
        [Reactive] internal string FileInputName { get; set; }
        [Reactive] internal DirectoryHierarchyViewModel SelectedDirectory { get; set; }
        [Reactive] internal FileViewModel SelectedFile { get; set; }
        [Reactive] internal PathSelectorViewModel[] Paths { get; set; }
        [Reactive] internal string LookupPath { get; set; }
        [Reactive] internal string LookupFile { get; set; }

        [Reactive] public string StartupLocation { get; set; }

        public string FileSelection { [ObservableAsProperty] get; }
        internal bool HasNoFile { [ObservableAsProperty] get; }

        internal ReactiveCommand<string , Unit> FindPathCommand { get; private set; }
        internal ReactiveCommand<DirectoryHierarchyViewModel , Unit> ListFilesCommand { get; private set; }

        private readonly ConcurrentBag<IDisposable> _cleanUps;

        public FileBrowserViewModel()
        {
            Activator = new ViewModelActivator();
            _cleanUps = new ConcurrentBag<IDisposable>();

            InitializeCommands( this );

            var filter = this.WhenAnyValue( x => x.LookupFile )
                .Select( BuildFilter );

            var filterRegex = this.WhenAnyValue( x => x.FilterRegex )
                .Select( BuildFilterRegex );

            this.WhenActivated( disposables =>
            {
                FindPathCommand.ThrownExceptions
                    .Subscribe( exc => this.Log().Error( "Couldn't find path: {0}" , exc ) )
                    .DisposeWith( disposables );

                ListFilesCommand.ThrownExceptions
                    .Subscribe( exc => this.Log().Error( "Couldn't list files: {0}" , exc ) )
                    .DisposeWith( disposables );

                DirectoriesCache.Connect()
                    .Throttle( TimeSpan.FromMilliseconds( 10 ) )
                    .Sort( SortExpressionComparer<DirectoryHierarchyViewModel>.Ascending( o => o.IsSpecialFolder ? 0 : 1 )
                        .ThenByAscending( o => o.Name ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _directories )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );

                FileCache.Connect()
                    .Filter( filterRegex )
                    .Filter( filter )
                    .Sort( SortExpressionComparer<FileViewModel>.Ascending( o => o.Name ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _files )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );

                FileCache.Connect()
                    .Throttle( TimeSpan.FromMilliseconds( 50 ) )
                    .Select( _ => FileCache.Count == 0 )
                    .ToPropertyEx( this , x => x.HasNoFile , true , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedDirectory )
                    .Do( _ =>
                    {
                        FileCache.Clear();
                        FileInputName = string.Empty;
                    } )
                    .WhereNotNull()
                    .Do( d =>
                    {
                        Paths = d.Paths.Select( p => new PathSelectorViewModel( this , p ) ).ToArray();
                        SelectedDirectory.IsSelected = true;

                        foreach ( var cleanUp in _cleanUps )
                            cleanUp.Dispose();
                        _cleanUps.Clear();
                    } )
                    .InvokeCommand( ListFilesCommand )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedFile )
                    .DistinctUntilChanged()
                    .WhereNotNull()
                    .Subscribe( f => FileInputName = f?.DisplayName ?? string.Empty )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.FileInputName )
                    .Throttle( TimeSpan.FromMilliseconds( 75 ) )
                    .DistinctUntilChanged()
                    .Select( fileName => Files.FirstOrDefault( x => string.Compare( x.DisplayName , fileName , StringComparison.CurrentCultureIgnoreCase ) == 0 ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Subscribe( f => SelectedFile = f )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedDirectory ).WhereNotNull()
                    .CombineLatest( this.WhenAnyValue( x => x.FileInputName ) )
                    .Select( t =>
                    {
                        var (directory, file) = t;
                        return Path.Combine( directory.Name , file );
                    } )
                    .ToPropertyEx( this , x => x.FileSelection , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.StartupLocation )
                    .Throttle( TimeSpan.FromMilliseconds( 50 ) )
                    .Where( x => !string.IsNullOrWhiteSpace( x ) && Directory.Exists( x ) )
                    .Subscribe( d =>
                    {
                        var info = new DirectoryInfo(d);
                        var root = DirectoriesCache.Items.FirstOrDefault( x => string.Compare( info.Root.FullName.NormalizePath() , x.Name.NormalizePath() , StringComparison.OrdinalIgnoreCase ) == 0 );

                        if ( root != null)
                        {
                            root.Expanded = true;
                            _cleanUps.Add(ExploreHierarchy(d,root));
                        }
                        
                    } )
                    .DisposeWith( disposables );
            } );

            Observable.Return( Unit.Default )
                .ObserveOn( RxApp.TaskpoolScheduler )
                .Subscribe( _ =>
                {
                    DirectoriesCache.AddOrUpdate( new DirectoryHierarchyViewModel { Name = Environment.GetFolderPath( Environment.SpecialFolder.Desktop ) , IsSpecialFolder = true , SpecialCategory = Category.Desktop } );
                    DirectoriesCache.AddOrUpdate( new DirectoryHierarchyViewModel { Name = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ) , IsSpecialFolder = true , SpecialCategory = Category.Documents } );
                    DirectoriesCache.AddOrUpdate( new DirectoryHierarchyViewModel { Name = Environment.GetFolderPath( Environment.SpecialFolder.MyPictures ) , IsSpecialFolder = true , SpecialCategory = Category.Pictures } );
                    DirectoriesCache.AddOrUpdate( new DirectoryHierarchyViewModel { Name = Environment.GetFolderPath( Environment.SpecialFolder.MyVideos ) , IsSpecialFolder = true , SpecialCategory = Category.Videos } );
                    DirectoriesCache.AddOrUpdate( new DirectoryHierarchyViewModel { Name = Environment.GetFolderPath( Environment.SpecialFolder.MyMusic ) , IsSpecialFolder = true , SpecialCategory = Category.Music } );
                    DirectoriesCache.AddOrUpdate( DriveInfo.GetDrives().Select( d =>
                        new DirectoryHierarchyViewModel { Name = d.Name , IsDrive = true } ) );
                } );
        }

        private IDisposable ExploreHierarchy( string targetPath , DirectoryHierarchyViewModel directory )
        {
            return directory.WhenAnyValue( x => x.Children )
                .WhereNotNull()
                .Subscribe( children =>
                {
                    directory.Expanded = true;
                    var fhvm = children.Select( fileHierarchyViewModel => (File: fileHierarchyViewModel, Cnt: targetPath.CommonStartingCharacters( fileHierarchyViewModel.Name )) )
                        .OrderByDescending( t => t.Cnt )
                        .First().File;

                    if ( string.Compare( targetPath.NormalizePath() , fhvm.Name.NormalizePath() ,
                        StringComparison.OrdinalIgnoreCase ) == 0 )
                        Observable.Return( fhvm )
                            .Throttle(TimeSpan.FromMilliseconds(100))
                            .ObserveOn( RxApp.MainThreadScheduler )
                            .Subscribe( f => SelectedDirectory = f );
                    else
                        _cleanUps.Add( ExploreHierarchy( targetPath , fhvm ) );
                } );
        }

        private static Func<FileViewModel , bool> BuildFilter( string lookup )
            => item => string.IsNullOrWhiteSpace( lookup ) || item?.DisplayName?.Contains( lookup , StringComparison.CurrentCultureIgnoreCase ) == true;

        private static Func<FileViewModel , bool> BuildFilterRegex( string regex )
            => item =>
                string.IsNullOrWhiteSpace( regex ) || Regex.Match( item?.Name , regex ).Success;

        private static void InitializeCommands( FileBrowserViewModel @this )
        {
            var canSearch = @this.WhenAnyValue( x => x.LookupPath )
                .Throttle( TimeSpan.FromMilliseconds( 50 ) )
                .Select( path => !string.IsNullOrWhiteSpace( path ) && Directory.Exists( path ) )
                .ObserveOn( RxApp.MainThreadScheduler );
            @this.FindPathCommand = ReactiveCommand.CreateFromObservable<string , Unit>( path => Observable.Start( () =>
            {
                var directoryInfo = new DirectoryInfo( path );

                var root = new DirectoryHierarchyViewModel { Name = directoryInfo.Root.FullName , IsDrive = true };
                @this.DirectoriesCache.AddOrUpdate( root );
                @this._cleanUps.Add( @this.ExploreHierarchy( path , root ) );
            } ) , canSearch );

            @this.ListFilesCommand = ReactiveCommand.CreateFromObservable<DirectoryHierarchyViewModel , Unit>( d => Observable.Start(
                () =>
                {
                    @this.FileCache.AddOrUpdate( Directory.EnumerateFiles( d.Name )
                        .Select( f => new FileViewModel { Name = f } ).ToArray() );

                } ) );

        }
    }
}